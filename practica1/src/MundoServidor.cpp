// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h" //Practica3
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <iostream>		//Modificaciones Practica2
#include <sys/types.h>
#include <sys/stat.h>
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h> 
#include <unistd.h>

#define TAM_BUFFER 60


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


void* hilo_comandos1(void* d);
void* hilo_comandos2(void* d);
void* hilo_conexiones(void* s); //P5

CMundo::CMundo()
{
	Init();
	acabar=0;
}

CMundo::~CMundo()
{
	char buff[]="Juego finalizado";
	write(fd, buff, sizeof(buff));
	close(fd);	//Practica2: se encarga de cerrar la tubería adecuadamente
	comunicacion.Close();
	//conexion.Close();
	//close(fd_coord);
	//close(fd_teclas);
	acabar=1;
    for(int i=0;i<conexiones.size();i++)
            conexiones[i].Close();
    //server.Close();
    //Esperamos a que terminen los hilos //P5
    pthread_join(&thid1,NULL);
    pthread_join(&thid2,NULL);
    pthread_join(&thid3,NULL);
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);

	char buffer[TAM_BUFFER];	//Practica2: recibe los datos de los puntos por la tuberia

	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
	sprintf(buffer,"Jugador 2 marca 1 punto, lleva un total de %d\n",puntos2);	//Practica2: salida por pantalla del logger
	write(fd,buffer,sizeof(buffer));//Practica2:escritura para el logger
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
	sprintf(buffer,"Jugador 1 marca 1 punto, lleva un total de %d\n",puntos1);	//Practica2: salida por pantalla del logger
	write(fd,buffer,sizeof(buffer));//Practica2: escritura para el logger

	}


	
	//el juego finaliza cuando uno de los dos jugadores alcanza 3 puntos
	if(puntos1>=3||puntos2>=3)
		exit(0);	
	// datos a enviar desde el servidor al cliente
	char cad[200];
	sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x,esfera.centro.y, jugador1.x1,jugador1.y1,jugador1.x2,
	jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2); //Practica3
	//write(fd_coord,cad,sizeof(cad));
	
	//Sockets
	
	comunicacion.Send(cad, 200);
	
	//Gestionar las desconexiones //P5
	for (i=conexiones.size()-1; i>=0; i--) {
         if (conexiones[i].Send(cad,200) <= 0) {
            conexiones.erase(conexiones.begin()+i);
            if(i<2){//Reinicia los puntos si el eliminado es alguno de los dos primeros
                puntos1=0;
                puntos2=0;
            }
         }
     }
  }
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	/*switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}*/
}

void CMundo::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	fd=open("/tmp/tuberia_logger",O_WRONLY);	//Practica2: abre la tuberia en modo escritura
	
	//mkfifo ("/tmp/Envio_coordenadas_servidorCliente",0777);
	//fd_coord=open ("/tmp/Envio_coordenadas_servidorCliente",O_WRONLY); //envio de las coordenadas 
	// al cliente desde el servidor
	
	//fd_teclas=open("/tmp/Envio_teclaspulsadas_clienteServidor",O_RDONLY); //lectura
	
	pthread_create(&thid1, NULL, hilo_comandos1, this); //creacion del thread
	pthread_create(&thid2, NULL, hilo_comandos2, this); //Jugador2 //P5
	pthread_create(&thid3, NULL, hilo_conexiones, this); //creación del thread para los clientes //P5
	
	//sockets
	char micadena[TAM_BUFFER];
	conexion.InitServer((char*)"127.0.0.1",3550);
	//192.168.1.104
	//conexion.InitServer((char*)"192.168.1.104",3550);
	
	comunicacion=conexion.Accept();
	comunicacion.Receive(micadena,TAM_BUFFER);
	printf("El nombre del cliente es: %s \n",micadena);
	
}

//Practica3:
void* hilo_comandos1(void* d)
{
      CMundo* p1=(CMundo*) d;
      p1->RecibeComandosJugador1();
}

void* hilo_comandos2(void* d) //P5
{
	CMundo* p2=(CMundo*) d;
	p2->RecibeComandosJugador2();
}

void* hilo_conexiones(void* s) //P5
{
	CMundo* c=(CMundo*) s;
	c->GestionaConexiones();
}

void CMundo::RecibeComandosJugador1()
{
     while (!=acabar) { //en cada iteracion lee de la tuberia la tecla pulsada 
		 // y modifica la velocidad del jugador correspondiente
            usleep(10);
            char cad[TAM_BUFFER];
            //read(fd_teclas, cad, sizeof(cad));
            comunicacion.Receive(cad,TAM_BUFFER);
            unsigned char key;
            sscanf(cad,"%c",&key);
            if(key=='s')jugador1.velocidad.y=-4;
            if(key=='w')jugador1.velocidad.y=4;
            //if(key=='l')jugador2.velocidad.y=-4;
            //if(key=='o')jugador2.velocidad.y=4;
      }
}

void CMundo::RecibeComandosJugador2()
{
     while (!=acabar) { //en cada iteracion lee de la tuberia la tecla pulsada 
		 // y modifica la velocidad del jugador correspondiente
            usleep(10);
            char cad[TAM_BUFFER];
            //read(fd_teclas, cad, sizeof(cad));
            comunicacion.Receive(cad,TAM_BUFFER);
            unsigned char key;
            sscanf(cad,"%c",&key);
            //if(key=='s')jugador1.velocidad.y=-4;
            //if(key=='w')jugador1.velocidad.y=4;
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
      }
}

void CMundo::GestionaConexiones()
{
    printf("Esperando clientes...");
    int i=0;
    /*while(!=acabar){
        char nombre[100];
        printf("Esperando el cliente numero: %i\n",++i);
        //Acepta un cliente y lo agrega al final
        conexiones.push_back(server.Accept());
        //recibe del ultimo cliente agregado su nombre
        conexiones[conexiones.size()-1].Receive(nombre,sizeof(nombre));
        //guarda el nombre del cliente agregado en el vector de nombres
        nombreClientes.push_back(nombre);
        if(nombreClientes.size()>=2){
            esfera.iniciar();
        }*/
        while(!(c->acabar))){
			Socket s=c->server.Accept();
			if(s.getSock()!=-1){
				c->conexion.push_back(s);
			}
        
    }
    printf("Fin de transmision...");
}
