#include <sys/types.h>
#include <sys/stat.h>
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h> 
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#define TAM_BUFFER 60

using namespace std;

int main(){
	int epipe=mkfifo("/tmp/tuberia_logger",0777);
	if(epipe==-1){
		perror("Fallo al crear la tuberia");
		exit(1);
	}
	int fd=open("/tmp/tuberia_logger",O_RDONLY);

	while(1){
		char buffer[TAM_BUFFER];
		int eread=read(fd,buffer,sizeof(buffer));
		if(eread==-1){
			perror("Fallo en la lectura de la tuberia");
			exit(1);
		}
		else if(eread==0){
			int eclose=close(fd);
				if(eclose==-1){
				perror("Fallo al cerrar la tuberia");
				exit(1);
			}
		unlink("/tmp/tuberia_logger");
		exit(0);
		}
		else{
			cout<<buffer<<endl;
		}
	}
	return 0;
}
	

