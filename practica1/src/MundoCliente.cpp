// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h" //Practica3
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <iostream>		//Modificaciones Practica2
#include <sys/types.h>
#include <sys/stat.h>
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h> 
#include <unistd.h>

#define TAM_BUFFER 60



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

char* org;

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	//munmap(org,sizeof(MemC));	//Practica2
	//close(fd);
	//unlink("/tmp/tuberia_logger");	//Practica2: se encarga de cerrar la tubería adecuadamente
	
	//close(fd_coord);
	//unlink("/tmp/Envio_coordenadas_servidorCliente");
	
	//close(fd_teclas);
	//unlink("/tmp/Envio_teclaspulsadas_clienteServidor");
	
	comunicacion.Close();
	munmap(org,sizeof(MemC));	//Practica2
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);

	char buffer[TAM_BUFFER];	//Practica2: recibe los datos de los puntos por la tuberia

	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
	//sprintf(buffer,"Jugador 2 marca 1 punto, lleva un total de %d\n",puntos2);	//Practica2: salida por pantalla del logger
	//write(fd,buffer,sizeof(buffer));//Practica2
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
	//sprintf(buffer,"Jugador 1 marca 1 punto, lleva un total de %d\n",puntos1);	//Practica2: salida por pantalla del logger
	//write(fd,buffer,sizeof(buffer));//Practica2

	}


	switch(pMemC->accion)
	{
		case 1:  OnKeyboardDown('w',0,0); break;
		case 0:  break;
		case -1: OnKeyboardDown('s',0,0); break;
	}

	
	
	//el juego finaliza cuando uno de los dos jugadores alcanza 3 puntos
	if(puntos1>=3||puntos2>=3)
		exit(0);	
	// lectura de los datos enviados a través de la tubería al cliente desde el servidor
	// e impresion de dichos datos
	
	char cad[200];
	//read(fd_coord, cad, sizeof(cad));
	
	//Socket
	comunicacion.Receive(cad,200);
	sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x,&esfera.centro.y, &jugador1.x1,&jugador1.y1,
	&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2); //Practica3
	
	pMemC->esfera=esfera;
	pMemC->raqueta1=jugador1;


}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}
	
	char cad[200];
	sprintf(cad,"%c",key);
	//write(fd_teclas,cad,sizeof(cad));
	comunicacion.Send(cad,TAM_BUFFER);
}

void CMundo::Init()
{
	//Socket
	char nombre [TAM_BUFFER];
	
	printf("Introduzca su nombre de cliente\n");
	scanf("%s",nombre);
	comunicacion.Connect((char*)"138.4.198.142",3550);
	comunicacion.Send(nombre,TAM_BUFFER);
	
	
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	//fd=open("/tmp/tuberia_logger",O_RONLY);	//Practica2: abre la tuberia en modo escritura
	
	int file=open("/tmp/datosComp.txt",O_RDWR|O_CREAT|O_TRUNC, 0666);

	write(file,&MemC,sizeof(MemC));

	org=(char*)mmap(NULL,sizeof(MemC),PROT_WRITE|PROT_READ,MAP_SHARED,file,0); //proyectamos en memoria mmap

	close(file);
	
	pMemC=(DatosMemCompartida*)org; //asigna la direccion de comienzo del puntero creado en bot.cpp
	
	pMemC->accion=0;
	/*
	mkfifo ("/tmp/Envio_coordenadas_servidorCliente",0777);
	fd_coord=open ("/tmp/Envio_coordenadas_servidorCliente",O_RDONLY); //tuberia de lectura de las coordenadas 
	//enviadas al cliente desde el servidor
	
	mkfifo ("/tmp/Envio_teclaspulsadas_clienteServidor",0777);
	fd_teclas=open("/tmp/Envio_teclaspulsadas_clienteServidor",O_WRONLY); //tuberia de escritura de las teclas
	//pulsadas del cliente al servidor
	*/
}
